<?php
session_start();

include("dbconfig.php");

if(isset($_POST['pass']))
{
    for($i = 0; $i<$_SESSION['i_order']; $i++){
            $ordID = $_SESSION['ordID'][$i];
            switch ($_REQUEST['pass']){
                    case "$i":
                    $sql = "UPDATE userorder SET status = '1'  WHERE ordID = '$ordID' ";
        
                   if ($conn->query($sql) === TRUE) {
                     $_SESSION['message'] = 'Record updated successfully';
                     header("location: orderManagement.php");
                   } else {
                         echo "Error: " . $sql . "<br>" . $conn->error;
                         break;
                     }
             }   
    }
    
}
elseif(isset($_POST['fail'])){
    for($i = 0; $i<$_SESSION['i_order']; $i++){
            $ordID = $_SESSION['ordID'][$i];
            switch ($_REQUEST['fail']){
                    case "$i":
                    $sql = "UPDATE userorder SET status = '-1'  WHERE ordID = '$ordID' ";
        
                   if ($conn->query($sql) === TRUE) {
                     $_SESSION['message'] = 'Record updated successfully';
                     header("location: orderManagement.php");
                   } else {
                         echo "Error: " . $sql . "<br>" . $conn->error;
                         break;
                     }
             }   
    }
    
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Order Detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    
</head>
<body>
<div class="container">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Order Detail</a>
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	<!-- left nav here -->
	      </ul>
             <ul class="nav navbar-nav navbar-right">
                 <a href="main.php?logout='1'" style="color: red;">logout</a> 
           </ul>
	    </div>
	  </div>
	</nav>
	<h1 class="page-header text-center">Order Detail</h1>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<form method="" >
			<table class="table table-bordered table-striped" id="table">
				<thead>
					<th>ProductName</th>
					<th>Price</th>
					<th>Quantity</th>
                    <th>Subtotal</th>
				</thead>
                
                <?php
                
                    for($i = 0; $i<$_SESSION['i_order']; $i++){
                        $ordID = $_SESSION['ordID'][$i];
                        $total = 0;
                        switch ($_REQUEST['detail']) {
                                case "$i":
                                $sql = "SELECT * FROM orderitem where ordID = '$ordID' ";
                                $query = $conn->query($sql);
                                
                                while($row = $query->fetch_assoc()){
                                    $prdID = $row['prdID'];
                                    
                                    $sql2 = "SELECT * FROM products where id = '$prdID'";
                                    $query2 = $conn->query($sql2);
                                    $row2 = $query2->fetch_assoc();
                                    
                                        
                        
                ?>
                               <tr>
                                   <td><?php echo $row2['name']; ?></td>
                                   <td><?php echo $row2['price']; ?></td>
                                   <td><?php echo $row['quantity']; ?></td>
                                   <td><?php echo number_format($row['quantity']*$row2['price'], 2); ?></td>
									<?php $total += $row['quantity']*$row2['price']; ?>
                               </tr>
                <?php
                            }
                ?>
                                <tr>
						           <td colspan="3" align="right"><b>Total</b></td>
						           <td><b><?php echo number_format($total, 2); ?></b></td>
					           </tr>
                <?php
                                break;
                        }
                    }
                ?>
                </table>
			</form>
		</div>
	</div>
</div>
</body>
</html>             