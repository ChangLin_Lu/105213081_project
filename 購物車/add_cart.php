<?php
	session_start();

    $proID = (int)$_GET['id'];
	//check if product is already in the cart
	if(!in_array($_GET['id'], $_SESSION['cart'])){ //in_array 用來判斷某個值是否存在陣列中 in_array ( 要比對的值 , 要比對的陣列)
		array_push($_SESSION['cart'], $_GET['id']);
		$_SESSION['message'] = 'Product added to cart';
	}
	else{
        $_SESSION['message'] = 'Product already in cart';
	}

	header('location: main.php');
?>