<?php
	session_start();
	unset($_SESSION['cart']);
    unset($_SESSION['address']);
	$_SESSION['message'] = 'Cart cleared successfully';
	header('location: main.php');
?>