<?php
session_start();

if(isset($_SESSION['logged'])){
   header("location: main.php");
   exit(); 
}
    


include("header.php");
?>
<body>
     <?php include('loginToServer.php'); include('errors.php'); ?>
        <div class="col-lg-6 col-12 pb-8 col offset-3">
            <div class="card h-100">
                <div class="card-body">
                    <h2 class="text-center mb-4">Login</h2>
                    <form class="py-2" role="form" method="post">
                        <div class="form-group">
                            <label for="inputUserNameForm" class="sr-only form-control-label">Username</label>
                            <div class="mx-auto col-sm-10">
                                <input type="text" class="form-control" name="inputUserNameForm" placeholder="UserName" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPasswordForm" class="sr-only form-control-label">Password</label>
                            <div class="mx-auto col-sm-10">
                                <input type="password" class="form-control" name="inputPasswordForm" placeholder="password" required="required" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10 pb-3 pt-2">
                                <button type="submit" class="btn btn-outline-secondary btn-lg btn-block" name="login_user">Login</button>
                                <a href="register.php" class="btn btn-outline-secondary btn-lg btn-block">Register</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</body>
