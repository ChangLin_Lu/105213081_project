<?php
session_start();

if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
    exit();
}
if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
    exit();
  }
if($_SESSION['role'] == 1){
    
}else{
    header('location: main.php');   
}

include("dbconfig.php");
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Product management</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    
</head>
<body>
<div class="container">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Product Management</a>
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	<!-- left nav here -->
	      </ul>
           <ul class="nav navbar-nav navbar-right">
                 <a href="main.php?logout='1'" style="color: red;">logout</a> 
           </ul>
	    </div>
	  </div>
	</nav>
	<h1 class="page-header text-center">Product Details</h1>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<?php 
			if(isset($_SESSION['message'])){
				?>
				<div class="alert alert-info text-center">
					<?php echo $_SESSION['message']; ?>
				</div>
				<?php
				unset($_SESSION['message']);
			}

			?>
			<form method="POST" action="AddAndEditProduct.php">
			<table class="table table-bordered table-striped" id="table">
				<thead>
					<th></th>
					<th>Name</th>
					<th>Price</th>
					<th>Image</th>
                    <th></th>
				</thead>
                
                <?php
				    $sql = "SELECT * FROM products ";
                    $query = $conn->query($sql);
                    $total_records=mysqli_num_rows($query);  // 取得記錄數
                    $i = 0;
                
                    while($row = $query->fetch_assoc()){
                    $proID = $row['id'];
                    $_SESSION['proID'][$i] = $proID;
                        
                ?>
                <tr>
				<td>
				    <button type="submit" class="btn btn-danger btn-sm" name="delete" value="<?php echo $i; ?>"><span class="glyphicon glyphicon-trash"></span></button>
				</td>
                <td><input type="text" class="form-control" value="<?php echo $row['name']; ?>" name="name_<?php echo $i; ?>"></td>
                <td><input type="text" class="form-control" value="<?php echo number_format($row['price'], 2); ?>" name="price_<?php echo $i; ?>"></td>
                <td><input type="text" class="form-control" value="<?php echo $row['photo']; ?>" name="image_<?php echo $i; ?>"></td>
                <td>
                    <button type="submit" class="btn btn-primary" name="save" value="<?php echo $i; ?>"><span class="glyphicon glyphicon-pencil"></span></button>
				</td>
                </tr>
                <?php
                        $i++;
                }
                $_SESSION['i'] = $i; 
                ?>
                
			</table>
			<button type="button" onclick = "addproduct()" class="btn btn-primary" name="addProduct">Add Product</button>
            <a href="orderManagement.php" class="btn btn-success" name="addProduct">Check Order</a>
			</form>
		</div>
	</div>
</div>
<script language="javascript">
    function addproduct() {
       var table = document.getElementById("table");

       // Create an empty <tr> element and add it to the 1st position of the table:
       var row = table.insertRow(1);

       // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
      var cell1 = row.insertCell(0);
      var cell2 = row.insertCell(1);
      var cell3 = row.insertCell(2);
      var cell4 = row.insertCell(3);
      var cell5 = row.insertCell(4);

     // Add some text to the new cells:
     cell1.innerHTML = '<button type="submit" class="btn btn-danger btn-sm" name="newdelete"><span class="glyphicon glyphicon-trash"></span></button>';
     cell2.innerHTML = '<input type="text" class="form-control" name="newname">';
     cell3.innerHTML = '<input type="text" class="form-control" name="newprice">';
     cell4.innerHTML = '<input type="text" class="form-control" name="newimage">';
     cell5.innerHTML = '<button type="submit" class="btn btn-primary" name="add"><span class="glyphicon glyphicon-file"></span></button>';  
    }
</script> 

</body>
</html>
