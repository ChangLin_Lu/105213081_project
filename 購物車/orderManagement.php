<?php
session_start();

if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
    exit();
}
if($_SESSION['role'] == 1){
    
}else{
    header('location: main.php');   
}

include("dbconfig.php");
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Order management</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    
</head>
<body>
<div class="container">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Order Management</a>
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	<!-- left nav here -->
	      </ul>
             <ul class="nav navbar-nav navbar-right">
                 <a href="main.php?logout='1'" style="color: red;">logout</a> 
           </ul>
	    </div>
	  </div>
	</nav>
	<h1 class="page-header text-center">Order list</h1>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<?php 
			if(isset($_SESSION['message'])){
				?>
				<div class="alert alert-info text-center">
					<?php echo $_SESSION['message']; ?>
				</div>
				<?php
				unset($_SESSION['message']);
			}

			?>
			<form method="POST" action="OrderdetailandCheck.php">
			<table class="table table-bordered table-striped" id="table">
				<thead>
					<th></th>
					<th>UserName</th>
					<th>OrderDate</th>
					<th>Address</th>
                    <th>status</th>
                    <th></th>
				</thead>
                
                <?php
				    $sql = "SELECT * FROM userorder ";
                    $query = $conn->query($sql);
                    $total_records=mysqli_num_rows($query);  // 取得記錄數
                    $i = 0;
                
                    while($row = $query->fetch_assoc()){
                        $ordID = $row['ordID'];
                        $_SESSION['ordID'][$i] = $ordID;
                        
                        $usrID = $row['uID'];
                        $sql2 = "SELECT * FROM users where id = '$usrID'";
                        $query2 = $conn->query($sql2);
                        $row2 = $query2->fetch_assoc();
                        
                        if($row['status']== '0')
                            $status = 'Uncheck';
                        elseif($row['status']== '1')
                            $status = 'Pass';
                        else
                            $status = 'Fail';
                        
                ?>
                <tr>
				<td>
				    <button type="submit" class="btn btn-success btn-sm" name="pass" value="<?php echo $i; ?>"><span class="glyphicon glyphicon-check"></span></button>
                    &nbsp;
                    <button type="submit" class="btn btn-danger btn-sm" name="fail" value="<?php echo $i; ?>"><span class="glyphicon glyphicon-remove"></span></button>
				</td>
                <td><?php echo $row2['username']; ?></td>
                <td><?php echo $row['orderDate']; ?></td>
                <td><?php echo $row['address']; ?></td>
                <td><?php echo $status; ?></td>
                <td>
                    <button href="orderDetailandCheck.php" class="btn btn-primary" name="detail" value ="<?php echo $i; ?>"><span class="glyphicon glyphicon-list-alt"></span></button>
				</td>
                </tr>
                <?php
                        $i++;
                }
                $_SESSION['i_order'] = $i; 
                ?>
                
			</table>
			</form>
		</div>
	</div>
</div>
</body>
</html>
