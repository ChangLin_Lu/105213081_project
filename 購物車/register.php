<?php
session_start();

if(isset($_SESSION['logged'])){
    header("location: main.php");
    exit();
}
include("header.php");
?>
<?php include('registerToServer.php'); include('errors.php'); ?>

<div class="col-lg-6 col-12 pb-8 col offset-3">
            <div class="card h-100">
                <div class="card-body">
                    <h2 class="text-center mb-4">Sign-up</h2>
                    
                    <form role="form" method = "post" action = "register.php">
                        <div class="form-group">
                            <label for="inputUsername" class="sr-only form-control-label">username</label>
                            <div class="mx-auto col-sm-10">
                                <input type="text" class="form-control" name="inputUsername" placeholder="username" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="sr-only form-control-label">email</label>
                            <div class="mx-auto col-sm-10">
                                <input type="text" class="form-control" name="inputEmail" placeholder="email" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="sr-only form-control-label">password</label>
                            <div class="mx-auto col-sm-10">
                                <input type="password" class="form-control" name="inputPassword" placeholder="password" required="required" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPasswordVerify" class="sr-only form-control-label">verify</label>
                            <div class="mx-auto col-sm-10">
                                <input type="password" class="form-control" name="inputPasswordVerify" placeholder="verify password" required="required" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10 pb-3 pt-2">
                                <button type="submit" class="btn btn-outline-secondary btn-lg btn-block" name="reg_user">Register</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10 pb-3 pt-2">
  		                     Already a member? <a href="login.php">Sign in</a>
  	                        </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>