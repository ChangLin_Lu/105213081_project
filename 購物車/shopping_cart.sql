-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 
-- 伺服器版本： 10.4.8-MariaDB
-- PHP 版本： 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `shopping_cart`
--

-- --------------------------------------------------------

--
-- 資料表結構 `orderitem`
--

CREATE TABLE `orderitem` (
  `serno` int(11) NOT NULL,
  `ordID` int(11) NOT NULL,
  `prdID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `orderitem`
--

INSERT INTO `orderitem` (`serno`, `ordID`, `prdID`, `quantity`) VALUES
(25, 31, 2, 7),
(26, 31, 7, 1),
(27, 31, 3, 5),
(28, 31, 4, 6),
(29, 31, 8, 9),
(30, 32, 2, 6),
(31, 32, 4, 7),
(32, 33, 2, 5),
(33, 33, 1, 2),
(34, 33, 3, 3),
(35, 33, 4, 1),
(36, 34, 2, 1),
(37, 34, 3, 1),
(38, 34, 4, 1),
(39, 35, 3, 1),
(40, 35, 7, 1),
(41, 35, 8, 1),
(42, 35, 1, 1),
(43, 35, 2, 1),
(44, 35, 5, 1),
(45, 35, 6, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `photo`) VALUES
(1, 'DELL Inspiron 15 7000 15.6', 899, 'images/1.jpg'),
(2, 'MICROSOFT Surface Pro 4 & Typecover - 128 GB', 799, 'images/2.jpg'),
(3, 'DELL Inspiron 15 5000 15.6', 599, 'images/3.jpg'),
(4, 'LENOVO Ideapad 320s-14IKB 14\" Laptop - Grey', 399, 'images/4.jpg'),
(5, 'ASUS Transformer Mini T102HA 10.1\" 2 in 1 - Silver', 549.99, 'images/5.jpg'),
(6, 'DELL Inspiron 15 5000 15', 449.99, 'images/6.jpg'),
(7, 'Asus Zenfone 6', 399, 'images/7.jpg'),
(8, 'Asus Zenfone 5z', 299, 'images/8.jpg'),
(9, 'Asus Zenfone 5z', 299, 'images/8.jpg'),
(10, 'Asus Zenfone 5z', 299, 'images/8.jpg'),
(11, 'ABC', 1000, 'images/9.jpg'),
(12, 'BBB', 3000, 'images/11.jpg');

-- --------------------------------------------------------

--
-- 資料表結構 `userorder`
--

CREATE TABLE `userorder` (
  `ordID` bigint(11) NOT NULL,
  `uID` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orderDate` date NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `userorder`
--

INSERT INTO `userorder` (`ordID`, `uID`, `orderDate`, `address`, `status`) VALUES
(31, '12', '2019-12-17', 'kkkkkkkkk', 1),
(32, '15', '2019-12-17', 'Chiayi City', 1),
(33, '17', '2019-12-18', 'ahhhhhhhhhh', 1),
(34, '17', '2019-12-18', 'fuck', -1),
(35, '17', '2019-12-18', 'fuck', -1);

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `role`) VALUES
(0, 'admin', 'admin@gmail.com', '0192023a7bbd73250516f069df18b500', 1),
(5, 'ChangLin', 'a0975709756@gmail.com', '1976d4135f57a9aa2bef21ab42dcb4f0', 0),
(6, 'Vivian', 'Vivian@gmail.com', '200820e3227815ed1756a6b531e7e0d2', 0),
(7, 'test01', 'test01@gmail.com', '1976d4135f57a9aa2bef21ab42dcb4f0', 0),
(8, 'test02', 'test02@gmail.com', '1976d4135f57a9aa2bef21ab42dcb4f0', 0),
(9, 'test03', 'test03@gmail.com', '1976d4135f57a9aa2bef21ab42dcb4f0', 0),
(12, 'test06', 'test06@gmail.com', '1976d4135f57a9aa2bef21ab42dcb4f0', 0),
(13, 'test10', 'test10@gmail.com', '1976d4135f57a9aa2bef21ab42dcb4f0', 0),
(15, 'test11', 'test11@gmail.com', '1976d4135f57a9aa2bef21ab42dcb4f0', 0),
(17, 'shinhwa', 'shinhwa@gmail.com', '2fe3ce32cf1b78108a574243e4c63820', 0);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `orderitem`
--
ALTER TABLE `orderitem`
  ADD PRIMARY KEY (`serno`);

--
-- 資料表索引 `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `userorder`
--
ALTER TABLE `userorder`
  ADD PRIMARY KEY (`ordID`);

--
-- 資料表索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `orderitem`
--
ALTER TABLE `orderitem`
  MODIFY `serno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `userorder`
--
ALTER TABLE `userorder`
  MODIFY `ordID` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
